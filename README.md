# Project : NavigationDrawer Test 1

### Purpose 

This project was created for test NavigationDrawer with action bar menu in one project.


### Project Info 

Project Location
```
/home/farhansadik/Downloads/NavAcMenu1
```



### Tests 

 - [x] Test 1 - Navigation Drawer + ActionBar menu <br>
 - [x] Test 2 - Try to add listview or something in mainactivity for test, check it. Is it work or not! <br>
 - [x] Test 3 - add scrollbar in navigation drawer <br>
 - [ ] ***Test 4 - Create another custom layout and put NavigationDrawer in it and show it in mainActivity layout.*** <br>
 - [x] Test 5 - try to add navigation drawer in other activity <br>

### Result 

- Test 1 - *Passed* <br>
- Test 2 - *Passed* 
> Added some elements in home activity and it's worked! 

- Test 3 - *Passed* <br>
- Test 4 - *Failed!* <br>
- Test 5 - *Passed* <br>

## Final Test and Result 
*Everything worked well. Test passed! no more test will be applied*

# The Project has been dissmissed! 



**Farhan Sadik**

